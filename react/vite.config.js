import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/

function getPort(){
  const mode = "development";
  process.env = {...process.env, ...loadEnv(mode, process.cwd())};
  return process.env.PORT
}

export default defineConfig({
  plugins: [react()],
  server: {
    port: getPort(),
  }
})
