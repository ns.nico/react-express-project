import React, {useState, useEffect} from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

import './App.css';

import NavBarComponent from './component/NavBarComponent/NavBarComponent';
import HomePage from './pages/HomePage/HomePage';
import SigInPage from './pages/SignInPage/SignInPage';
import RegisterProduct from './pages/RegisterProduct/RegisterProduct';
import GuardedRoute from './guards/GuardedRoute';
import DataContext from './context/DataContext';
import Products from './pages/Products/Products';

function App() {
  const [isAuth, setIsAuth] = useState(false);

  useEffect(()=>{
    sessionStorage.getItem('auth')? setIsAuth(true): setIsAuth(false);
  });

  return (
    <>
      <Router>
        <DataContext.Provider value={{isAuth, setIsAuth}}>
          <NavBarComponent/>
          <Routes>
            <Route exact path='/' element={<HomePage/>}></Route>
            <Route exact path='/signin' element={<SigInPage/>}></Route>
            <Route exact path='/products' element={<GuardedRoute> <Products/> </GuardedRoute>} />
            <Route exact path='/registerproduct' element={<GuardedRoute> <RegisterProduct/> </GuardedRoute>} />
          </Routes>
        </DataContext.Provider>
      </Router>
    </>
  );
}

export default App;
