import React, { useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";

import './NavBarComponent.css'

import DataContext from "../../context/DataContext";

export default function NavBarComponent(){
  const { isAuth, setIsAuth } = useContext(DataContext);
  const [menuMobileOpen, setMenuMobileOpen] = useState(false);
  const navigate = useNavigate();

  function login(){
    isAuth? signout(): navigate('/signin');
  }
  function signout(){
    sessionStorage.clear();
    setIsAuth(false);
    navigate('/');
  }

  return (
  <>
    <nav>
      <ul className="container">
        <div className="menu">Money</div>
        <li className={`menu ${menuMobileOpen?'show':'hide'}`}>
          <Link to="/">Inicio</Link>
        </li>
        <li className={`menu ${menuMobileOpen?'show':'hide'}`}>
          <Link to="/products">Productos</Link>
        </li>
        <li className={`menu ${menuMobileOpen?'show':'hide'}`}>
          <Link to="/registerproduct">Agregar Producto</Link>
        </li>
        <li className={`menu ${menuMobileOpen?'show':'hide'}`}>
          <div className="menu-signin" onClick={login} >{isAuth?'Cerrar Sesion':'Iniciar Sesión'}</div>
        </li>
        <div className="mobile-menu">
          <span className={`menu-icon open ${menuMobileOpen? 'hide':'show'}`} onClick={()=>setMenuMobileOpen(true)}>&#x2261;</span>
          <span className={`menu-icon close ${menuMobileOpen?'show': 'hide'}`} onClick={()=>setMenuMobileOpen(false)}>&#x2715;</span>
        </div>
      </ul>
    </nav>
  </>
  );
}
