import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import DataContext from '../context/DataContext';

const GuardedRoute = ({children}) => {
    const { isAuth } = useContext(DataContext);
    return( isAuth? children : <Navigate to='/'/> );
}

export default GuardedRoute;