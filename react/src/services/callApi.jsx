import { existsData, getData } from './SessionStorage';

async function callLogin(url, body){
    try {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
               'Content-type': 'application/json; charset=UTF-8'
            },
        })
        return response;
      } catch (e) {
        throw e
      }
}
async function callApiGet(url){
  try {
      if(!existsData('auth')) return;
      const { accessToken } = await getData('auth');
      const response = await fetch(url, {
          method: 'GET',
          headers: {
             'Content-type': 'application/json; charset=UTF-8',
             'Authorization': `Bearer ${accessToken}`
          },
      });
      return response;
    } catch (e) {
      console.log('sin autorización')
    }
}

async function callApiPost(url, body){
  try {
      if(!existsData('auth')) return;
      const { accessToken } = await getData('auth');
      const response = await fetch(url, {
          method: 'POST',
          body: JSON.stringify(body),
          headers: {
             'Content-type': 'application/json; charset=UTF-8',
             'Authorization': `Bearer ${accessToken}`
          },
      })
      return response;
    } catch (e) {
      throw e
    }
}

export {callApiPost, callApiGet, callLogin};