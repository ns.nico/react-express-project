async function saveData(key, data){
    sessionStorage.setItem(key, JSON.stringify(data));
}

async function getData(key){
    const data = sessionStorage.getItem(key);
    return JSON.parse(data);
}

async function existsData(key){
    const data = sessionStorage.getItem(key);
    return Boolean(data);
}

export {saveData, getData, existsData};
