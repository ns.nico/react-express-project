import { useRef } from 'react';

import './RegisterProduct.css'

import { callApiPost } from '../../services/callApi';

export default function RegisterProduct(){
    const url = 'http://localhost:3000/api/registerProduct';
    const name = useRef();
    const presentation = useRef();
    const brand = useRef();
    const units = useRef();
    const totalPrice = useRef();
    const unitPrice = useRef();

    const handlerSubmit = async (e)=>{
        e.preventDefault();
        const body = {
          name: name.current.value,
          presentation: presentation.current.value,
          brand: brand.current.value,
          units: units.current.value,
          totalPrice: totalPrice.current.value,
          unitPrice: unitPrice.current.value
        };
        const res = await callApiPost(url, body);
        console.log(await res.json());
        e.target.reset();
    }
    return(
        <>
          <div className="register-product">
            <div className="container-register-product">
              <h3>Agregar Producto</h3>
              <form className="form-register-product" onSubmit={handlerSubmit}>
                <label>Nombre Producto</label>
                <input type="text" placeholder="Nombre Producto" ref={name}/>
                <label>Presentación</label>
                <input type="text" placeholder="Presentación" ref={presentation}/>
                <label>Marca</label>
                <input type="text" placeholder="Marca" ref={brand}/>
                <label>Cantidad</label>
                <input type="number" placeholder="Cantidad" ref={units}/>
                <label>Precio Total</label>
                <input type="number" placeholder="Precio Total" step='0.1' ref={totalPrice}/>
                <label>Precio Unidad</label>
                <input type="number" placeholder="Precio Unidad" step='0.1' ref={unitPrice}/>
                <button type="submit"> Agregar </button>
              </form>
            </div>
          </div>
        </>
    );
}