import {useEffect, useState, useCallback} from 'react';
import shortid from 'shortid';
import './Products.css';

import {callApiGet} from '../../services/callApi';

export default function Products(){
    const url = 'http://localhost:3000/api/products';
    const [products, setProducts] = useState([]);

    const loadingProducts = useCallback(async ()=>{
        const loadedProducts = await getProducts();
        loadedProducts?setProducts(loadedProducts.products):console.log('empty');
    },[]);

    const getProducts = async ()=>{
        const response = await callApiGet(url);
        return response ? response.json(): {};
    };
    useEffect(() => {
        loadingProducts();
    }, [loadingProducts]);

    return(
        <>
            <div className='products'>
                <div className='products-container'>
                    <h3>Lista de Productos</h3>
                    <div className='table'>
                        <div className="row-title-product">
                            <div>Nombre</div>
                            <div>Marca</div>
                            <div>Cantidad</div>
                        </div>
                        {rowProducts()}
                    </div>
                </div>
            </div>
        </>
    );

    function rowProducts(){
        if(!Boolean(products?.length)){
            return <></>;
        }
        const rows = products.map((product)=>{
            const productId = shortid.generate();
            const stock = product.stock.reduce((acc, ac)=>{return acc + ac.units}, 0)
            return(
                <div key={productId} className="row-product">
                    <div className='name-product'>{product.name}</div>
                    <div className='brand-product'>{product.brand}</div>
                    <div className='presentation-product'>{stock}</div>
                </div>
                );
            });
        return rows;
    }
}
