import { useRef, useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import './SignInPage.css';

import { callLogin } from '../../services/callApi';
import { saveData } from '../../services/SessionStorage';
import DataContext from '../../context/DataContext';
import messages from './messages/messages';

export default function SigInPage(){

    const url = 'http://localhost:3000/api/auth';
    const navigate = useNavigate();

    const email = useRef();
    const password = useRef();

    const [errorMessage, setErrorMessage] = useState('');
    const { setIsAuth } = useContext(DataContext);

    async function handlerSubmit(e){
        e.preventDefault();
        const response = await callLogin(url, {
            email: email.current.value,
            password: password.current.value});
        isValidLogin(response)
        e.target.reset();
    }

    const isValidLogin = async (response)=>{
        const data = await response.json();
        if (response.ok) {
          saveData('auth', data);
          setIsAuth(true);
          navigate('/');
        }else{
            setErrorMessage(messages[data.status]);
        }
    }

    return (
        <div className="signin">
            <div className='container-signin'>
                <h3>Inicio de Sesión</h3>
                <form className='form-signin' onSubmit={handlerSubmit}>
                    <label >Email</label>
                    <input type="email" placeholder='Email' ref={email}/>
                    <label > Contraseña</label>
                    <input type="password" placeholder='Contraseña' ref={password}/>
                    <label className={`error ${Boolean(errorMessage)?'':'hidden'}`}>{errorMessage}</label>
                    <button type="submit">Iniciar</button>
                </form>
            </div>
        </div>
    );
}