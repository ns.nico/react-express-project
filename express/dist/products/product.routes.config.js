"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductRoutes = void 0;
const express_validator_1 = require("express-validator");
const common_routes_config_1 = require("../common/common.routes.config");
const body_validation_middleware_1 = __importDefault(require("../common/middleware/body.validation.middleware"));
const products_middleware_1 = __importDefault(require("./middleware/products.middleware"));
const token_middleware_1 = __importDefault(require("../auth/middleware/token.middleware"));
const product_controller_1 = __importDefault(require("./controllers/product.controller"));
class ProductRoutes extends common_routes_config_1.CommonRoutesConfig {
    constructor(app) {
        super(app, 'Product routes');
    }
    configureRoutes() {
        this.app.route('/api/registerProduct')
            .post((0, express_validator_1.body)('name').isString(), (0, express_validator_1.body)('presentation').isString(), (0, express_validator_1.body)('brand').isString(), (0, express_validator_1.body)('units').isInt(), (0, express_validator_1.body)('totalPrice').isFloat(), (0, express_validator_1.body)('unitPrice').isFloat(), body_validation_middleware_1.default.verifyBodyFieldsErrors, token_middleware_1.default.validTokenNeeded, products_middleware_1.default.validateRequiredProductBodyFields, product_controller_1.default.addProduct);
        this.app.route('/api/products')
            .get(token_middleware_1.default.validTokenNeeded, products_middleware_1.default.validateRequiredGetProductFields, product_controller_1.default.getAllProducts);
        return this.app;
    }
}
exports.ProductRoutes = ProductRoutes;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdC5yb3V0ZXMuY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3Byb2R1Y3RzL3Byb2R1Y3Qucm91dGVzLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFFQSx5REFBeUM7QUFFekMseUVBQW9FO0FBQ3BFLGlIQUF1RjtBQUN2RiwyRkFBaUU7QUFDakUsMkZBQWtFO0FBQ2xFLDBGQUFpRTtBQUVqRSxNQUFhLGFBQWMsU0FBUSx5Q0FBa0I7SUFDakQsWUFBWSxHQUF3QjtRQUNoQyxLQUFLLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDakMsQ0FBQztJQUNELGVBQWU7UUFDWCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQzthQUNqQyxJQUFJLENBQ0QsSUFBQSx3QkFBSSxFQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUN2QixJQUFBLHdCQUFJLEVBQUMsY0FBYyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQy9CLElBQUEsd0JBQUksRUFBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFDeEIsSUFBQSx3QkFBSSxFQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUNyQixJQUFBLHdCQUFJLEVBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQzVCLElBQUEsd0JBQUksRUFBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFDM0Isb0NBQXdCLENBQUMsc0JBQXNCLEVBQy9DLDBCQUFlLENBQUMsZ0JBQWdCLEVBQ2hDLDZCQUFpQixDQUFDLGlDQUFpQyxFQUNuRCw0QkFBaUIsQ0FBQyxVQUFVLENBQy9CLENBQUM7UUFFTixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUM7YUFDMUIsR0FBRyxDQUNBLDBCQUFlLENBQUMsZ0JBQWdCLEVBQ2hDLDZCQUFpQixDQUFDLGdDQUFnQyxFQUNsRCw0QkFBaUIsQ0FBQyxjQUFjLENBQ25DLENBQUM7UUFDTixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQztDQUNKO0FBM0JELHNDQTJCQyJ9