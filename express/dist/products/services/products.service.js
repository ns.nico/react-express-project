"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const products_operations_1 = __importDefault(require("../operations/products.operations"));
class ProductsService {
    constructor() { }
    newProduct(resource) {
        return __awaiter(this, void 0, void 0, function* () {
            return products_operations_1.default.newProduct(resource);
        });
    }
    findProduct(resource) {
        return __awaiter(this, void 0, void 0, function* () {
            return products_operations_1.default.findProduct(resource);
        });
    }
    addProduct(resource) {
        return __awaiter(this, void 0, void 0, function* () {
            return products_operations_1.default.addProduct(resource);
        });
    }
    getAllProducts(resource) {
        return __awaiter(this, void 0, void 0, function* () {
            return products_operations_1.default.findProducts(resource);
        });
    }
}
exports.default = new ProductsService();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdHMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wcm9kdWN0cy9zZXJ2aWNlcy9wcm9kdWN0cy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsNEZBQW1FO0FBRW5FLE1BQU0sZUFBZTtJQUNqQixnQkFBYyxDQUFDO0lBQ1QsVUFBVSxDQUFDLFFBQTJCOztZQUN4QyxPQUFPLDZCQUFrQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNuRCxDQUFDO0tBQUE7SUFDSyxXQUFXLENBQUMsUUFBMkI7O1lBQ3pDLE9BQU8sNkJBQWtCLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BELENBQUM7S0FBQTtJQUNLLFVBQVUsQ0FBQyxRQUEyQjs7WUFDeEMsT0FBTyw2QkFBa0IsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkQsQ0FBQztLQUFBO0lBQ0ssY0FBYyxDQUFDLFFBQTBCOztZQUMzQyxPQUFPLDZCQUFrQixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNyRCxDQUFDO0tBQUE7Q0FDSjtBQUVELGtCQUFlLElBQUksZUFBZSxFQUFFLENBQUMifQ==