"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const shortid_1 = __importDefault(require("shortid"));
const mongoose_service_1 = __importDefault(require("../../common/services/mongoose.service"));
class ProductsOperations {
    constructor() {
        this.Schema = mongoose_service_1.default.getMongoose().Schema;
        this.stockSchema = new this.Schema({ units: Number,
            totalPrice: Number,
            unitPrice: Number,
            time: { type: Date, default: Date.now }
        }, { _id: false });
        this.productSchema = new this.Schema({
            _id: String,
            name: String,
            presentation: String,
            brand: String,
            stock: [this.stockSchema],
            product_user_id: String
        }, { id: false });
        this.Product = mongoose_service_1.default.getMongoose().model('Product', this.productSchema);
    }
    addProduct(productFields) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name, presentation, brand, product_user_id } = productFields, units = __rest(productFields, ["name", "presentation", "brand", "product_user_id"]);
            const product = yield this.Product.findOneAndUpdate({ name, presentation, brand, product_user_id }, { $push: { stock: units } }).exec();
            return product;
        });
    }
    findProduct(productFields) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name, presentation, brand, product_user_id } = productFields;
            const product = yield this.Product.findOne({ name, presentation, brand, product_user_id }).exec();
            return product;
        });
    }
    newProduct(productFields) {
        const _id = shortid_1.default.generate();
        const { name, presentation, brand, product_user_id } = productFields, units = __rest(productFields, ["name", "presentation", "brand", "product_user_id"]);
        const product = new this.Product({
            _id,
            name,
            presentation,
            brand,
            stock: [units],
            product_user_id
        });
        product.save();
    }
    findProducts(productField) {
        return this.Product.find(Object.assign({}, productField));
    }
}
exports.default = new ProductsOperations();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdHMub3BlcmF0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wcm9kdWN0cy9vcGVyYXRpb25zL3Byb2R1Y3RzLm9wZXJhdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHNEQUE4QjtBQUU5Qiw4RkFBcUU7QUFHckUsTUFBTSxrQkFBa0I7SUFNcEI7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLDBCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsS0FBSyxFQUFFLE1BQU07WUFDN0MsVUFBVSxFQUFFLE1BQU07WUFDbEIsU0FBUyxFQUFFLE1BQU07WUFDakIsSUFBSSxFQUFHLEVBQUUsSUFBSSxFQUFHLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRTtTQUN4QyxFQUFFLEVBQUMsR0FBRyxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDakMsR0FBRyxFQUFFLE1BQU07WUFDWCxJQUFJLEVBQUUsTUFBTTtZQUNaLFlBQVksRUFBRSxNQUFNO1lBQ3BCLEtBQUssRUFBRSxNQUFNO1lBQ2IsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN6QixlQUFlLEVBQUUsTUFBTTtTQUN0QixFQUFFLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRywwQkFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7SUFFSyxVQUFVLENBQUMsYUFBZ0M7O1lBQzdDLE1BQU0sRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxlQUFlLEtBQWUsYUFBYSxFQUF2QixLQUFLLFVBQUssYUFBYSxFQUF2RSxvREFBdUQsQ0FBZ0IsQ0FBQztZQUM5RSxNQUFNLE9BQU8sR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQy9DLEVBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFDLEVBQzVDLEVBQUMsS0FBSyxFQUFFLEVBQUMsS0FBSyxFQUFFLEtBQUssRUFBQyxFQUFDLENBQ3RCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDYixPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDO0tBQUE7SUFFSyxXQUFXLENBQUMsYUFBZ0M7O1lBQzlDLE1BQU0sRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUMsR0FBRyxhQUFhLENBQUM7WUFDbkUsTUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FDdEMsRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUMsQ0FDM0MsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNiLE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUM7S0FBQTtJQUVELFVBQVUsQ0FBQyxhQUFnQztRQUN2QyxNQUFNLEdBQUcsR0FBRyxpQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9CLE1BQU0sRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxlQUFlLEtBQWUsYUFBYSxFQUF2QixLQUFLLFVBQUssYUFBYSxFQUF2RSxvREFBdUQsQ0FBZ0IsQ0FBQztRQUM5RSxNQUFNLE9BQU8sR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQzVCO1lBQ0ksR0FBRztZQUNILElBQUk7WUFDSixZQUFZO1lBQ1osS0FBSztZQUNMLEtBQUssRUFBQyxDQUFDLEtBQUssQ0FBQztZQUNiLGVBQWU7U0FDbEIsQ0FDSixDQUFDO1FBQ0YsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFDRCxZQUFZLENBQUMsWUFBOEI7UUFDdkMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksbUJBQUssWUFBWSxFQUFFLENBQUM7SUFDaEQsQ0FBQztDQUNKO0FBQ0Qsa0JBQWUsSUFBSSxrQkFBa0IsRUFBRSxDQUFDIn0=