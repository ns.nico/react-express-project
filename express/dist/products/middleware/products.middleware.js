"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ProductMiddleware {
    constructor() { }
    validateRequiredProductBodyFields(req, res, next) {
        if (req.body.name &&
            req.body.presentation &&
            req.body.brand &&
            req.body.units &&
            req.body.totalPrice &&
            req.body.unitPrice) {
            req.body.product_user_id = req.body.userId;
            delete req.body.userId;
            delete req.body.email;
            next();
        }
        else {
            res.status(400).send({ error: "campo/s incorrecto/s" });
        }
    }
    validateRequiredGetProductFields(req, res, next) {
        req.body.product_user_id = req.body.userId;
        delete req.body.userId;
        next();
    }
}
exports.default = new ProductMiddleware();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdHMubWlkZGxld2FyZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wcm9kdWN0cy9taWRkbGV3YXJlL3Byb2R1Y3RzLm1pZGRsZXdhcmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSxNQUFNLGlCQUFpQjtJQUNuQixnQkFBYyxDQUFDO0lBQ2YsaUNBQWlDLENBQzdCLEdBQW9CLEVBQ3BCLEdBQXFCLEVBQ3JCLElBQTBCO1FBQzFCLElBQ0ksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZO1lBQ3JCLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSztZQUNkLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSztZQUNkLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVTtZQUNuQixHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFDckI7WUFDRyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUMzQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ3ZCLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDdEIsSUFBSSxFQUFFLENBQUM7U0FDVjthQUFJO1lBQ0QsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBQyxLQUFLLEVBQUMsc0JBQXNCLEVBQUMsQ0FBQyxDQUFBO1NBQ3ZEO0lBQ0wsQ0FBQztJQUNELGdDQUFnQyxDQUM1QixHQUFvQixFQUNwQixHQUFxQixFQUNyQixJQUEwQjtRQUMxQixHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMzQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLElBQUksRUFBRSxDQUFDO0lBQ1gsQ0FBQztDQUNKO0FBQ0Qsa0JBQWUsSUFBSSxpQkFBaUIsRUFBRSxDQUFDIn0=