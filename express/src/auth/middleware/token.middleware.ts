import express from 'express';
import jwt from 'jsonwebtoken';
import { JwtAccount } from '../../common/types/jwt';


class TokenMiddleware {
    validTokenNeeded(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        if (req.headers['authorization']) {
            try {
                const authorization = req.headers['authorization'].split(' ');
                if (authorization[0] !== 'Bearer') {
                    return res.status(401).send();
                } else {
                    const jwtSecret: string = process.env.JWT_SECRET || 'money';
                    const jwtData = jwt.verify(
                        authorization[1],
                        jwtSecret
                    ) as JwtAccount;
                    req.body.userId = jwtData.userId;
                    req.body.email = jwtData.email;
                    next();
                }
            } catch (err) {
                return res.status(403).send();
            }
        } else {
            return res.status(401).send();
        }
    }
}

export default new TokenMiddleware();