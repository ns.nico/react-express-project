import express from 'express';
import jwt from 'jsonwebtoken';

import debug from 'debug';
const log: debug.IDebugger = debug('app: auth-controller');


class AuthController {
    async createToken(req: express.Request, res: express.Response) {
        try {
            const jwtSecret: string = process.env.JWT_SECRET || "money";
            log(jwtSecret);
            const tokenExpirationInSeconds = 36000;
            const token = jwt.sign(req.body, jwtSecret, { expiresIn: tokenExpirationInSeconds });
            res.status(201).send({ accessToken: token });
        } catch (err) {
            res.status(500).send();
        }
    }
}

export default new AuthController();