import { CreateProductType, FindProductsType } from "../types/create.product.types";
import productsOperations from "../operations/products.operations";

class ProductsService{
    constructor(){}
    async newProduct(resource: CreateProductType){
        return productsOperations.newProduct(resource);
    }
    async findProduct(resource: CreateProductType){
        return productsOperations.findProduct(resource);
    }
    async addProduct(resource: CreateProductType){
        return productsOperations.addProduct(resource);
    }
    async getAllProducts(resource: FindProductsType){
        return productsOperations.findProducts(resource);
    }
}

export default new ProductsService();
