import shortid from "shortid";
import { CreateProductType, FindProductsType } from "../types/create.product.types";
import mongooseService from "../../common/services/mongoose.service";


class ProductsOperations{
    private Schema;
    private stockSchema;
    private productSchema;
    private Product;
    
    constructor(){
        this.Schema = mongooseService.getMongoose().Schema;
        this.stockSchema = new this.Schema({units: Number,
            totalPrice: Number,
            unitPrice: Number,
            time : { type : Date, default: Date.now }
            }, {_id: false});
        this.productSchema = new this.Schema({
            _id: String,
            name: String,
            presentation: String,
            brand: String,
            stock: [this.stockSchema],
            product_user_id: String
            }, { id: false });
        this.Product = mongooseService.getMongoose().model('Product', this.productSchema);
    }

    async addProduct(productFields: CreateProductType){
        const {name, presentation, brand, product_user_id, ...units } = productFields;
        const product = await this.Product.findOneAndUpdate(
            {name, presentation, brand, product_user_id},
            {$push: {stock: units}}
            ).exec();
        return product;
    }

    async findProduct(productFields: CreateProductType){
        const {name, presentation, brand, product_user_id} = productFields;
        const product = await this.Product.findOne(
            {name, presentation, brand, product_user_id}
            ).exec();
        return product;
    }

    newProduct(productFields: CreateProductType){
        const _id = shortid.generate();
        const {name, presentation, brand, product_user_id, ...units } = productFields;
        const product = new this.Product(
            {
                _id,
                name,
                presentation,
                brand,
                stock:[units],
                product_user_id
            }
        );
        product.save();
    }
    findProducts(productField: FindProductsType){
        return this.Product.find({...productField});
    }
}
export default new ProductsOperations();
