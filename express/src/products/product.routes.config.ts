import express from 'express';

import { body } from 'express-validator';

import { CommonRoutesConfig } from '../common/common.routes.config';
import bodyValidationMiddleware from '../common/middleware/body.validation.middleware';
import ProductMiddleware from './middleware/products.middleware';
import tokenMiddleware from '../auth/middleware/token.middleware';
import ProductController from './controllers/product.controller';

export class ProductRoutes extends CommonRoutesConfig{
    constructor(app: express.Application){
        super(app, 'Product routes');
    }
    configureRoutes(): express.Application {
        this.app.route('/api/registerProduct')
            .post(
                body('name').isString(),
                body('presentation').isString(),
                body('brand').isString(),
                body('units').isInt(),
                body('totalPrice').isFloat(),
                body('unitPrice').isFloat(),
                bodyValidationMiddleware.verifyBodyFieldsErrors,
                tokenMiddleware.validTokenNeeded,
                ProductMiddleware.validateRequiredProductBodyFields,
                ProductController.addProduct
            );

        this.app.route('/api/products')
            .get(
                tokenMiddleware.validTokenNeeded,
                ProductMiddleware.validateRequiredGetProductFields,
                ProductController.getAllProducts
            );
        return this.app;
    }
}
