import express from 'express';


class ProductMiddleware{
    constructor(){}
    validateRequiredProductBodyFields(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction){
        if (
            req.body.name &&
            req.body.presentation &&
            req.body.brand &&
            req.body.units &&
            req.body.totalPrice &&
            req.body.unitPrice
        ){
            req.body.product_user_id = req.body.userId;
            delete req.body.userId;
            delete req.body.email;
            next();
        }else{
            res.status(400).send({error:"campo/s incorrecto/s"})
        }
    }
    validateRequiredGetProductFields(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction){
        req.body.product_user_id = req.body.userId;
        delete req.body.userId;
        next();
    }
}
export default new ProductMiddleware();
