export interface CreateProductType{
    id: string,
    name: string,
    presentation: string,
    brand: string,
    units: number,
    totalPrice: number,
    unitPrice: number,
    product_user_id: string
}

export interface FindProductsType{
    product_user_id: string
}
