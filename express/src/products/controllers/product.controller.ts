import express from 'express';

import productsService from '../services/products.service';

class ProductController{
    constructor(){}
    async addProduct(req: express.Request, res: express.Response){
        const product = await productsService.findProduct(req.body);
        Boolean(product)? await productsService.addProduct(req.body): await productsService.newProduct(req.body);
        res.status(201).send({ status: 'El producto fue agregado' });
    }
    async getAllProducts(req: express.Request, res: express.Response){
        const products = await productsService.getAllProducts(req.body);
        res.status(200).send({products});
    }
}

export default new ProductController();