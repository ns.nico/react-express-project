import express from 'express';
import userService from '../services/users.service';

class UsersMiddleware {
    async validateSameEmailDoesntExist(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const user = await userService.getUserByEmail(req.body.email);
        if (user) {
            res.status(400).send({ status: `El email ya existe` });
            return;
        }
        next();
    }
}

export default new UsersMiddleware();
