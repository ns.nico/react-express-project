export interface CreateUserType {
    id: string;
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
    ci: number;
    permissionLevel?: number;
}
