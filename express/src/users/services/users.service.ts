import UsersOperations from '../operations/users.operations';
import { CreateUserType } from '../types/user.types';

class UsersService {
    async create(resource: CreateUserType) {
        return UsersOperations.addUser(resource);
    }
    async getUserByEmail(email: string) {
        return UsersOperations.getUserByEmail(email);
    }
    async getUserByEmailWithPassword(email: string) {
        return UsersOperations.getUserByEmailWithPassword(email);
    }
}

export default new UsersService();
