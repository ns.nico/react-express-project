import shortid from 'shortid';

import { CreateUserType } from '../types/user.types';
import mongooseService from '../../common/services/mongoose.service';


class UsersOperations {
    private userSchema;
    private User;
    private Schema;

    constructor() {
        this.Schema = mongooseService.getMongoose().Schema;
        this.userSchema = new this.Schema({
            _id: String,
            email: String,
            password: { type: String, select: false },
            firstName: String,
            lastName: String,
            ci: Number,
            permissionFlags: Number,
        }, { id: false });

        this.User = mongooseService.getMongoose().model('Users', this.userSchema);
    }

    async addUser(userFields: CreateUserType) {
        const userId = shortid.generate();
        const user = new this.User({
            _id: userId,
            ...userFields,
            permissionFlags: 1,
        });
        await user.save();
        return userId;
    }

    async getUserByEmail(email: string) {
        return this.User.findOne({ email: email }).exec();
    }

    async getUserByEmailWithPassword(email: string) {
        return this.User.findOne({ email: email })
            .select('_id email +password')
            .exec();
    }
}

export default new UsersOperations();
