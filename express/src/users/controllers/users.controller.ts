import argon2 from 'argon2';
import express from 'express';

import usersService from '../services/users.service';


class UsersController {
    async createUser(req: express.Request, res: express.Response) {
        req.body.password = await argon2.hash(req.body.password);
        req.body.ci = Number(req.body.ci);
        await usersService.create(req.body);
        res.status(201).send({ status: "usuario creado" });
    }
}

export default new UsersController();