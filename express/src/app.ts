import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import * as http from 'http';

import { AuthRoutes } from './auth/auth.routes.config';
import { CommonRoutesConfig } from './common/common.routes.config';
import { UsersRoutes } from './users/users.routes.config';
import { PagesRoutes } from './pages/pages.routes.config';
import { ProductRoutes } from './products/product.routes.config';


import debug from 'debug';
const log: debug.IDebugger = debug('app');

dotenv.config();

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port: number = Number(process.env.PORT) || 3000;
const routes: Array<CommonRoutesConfig> = [];

app.use(express.json());
app.use(cors());

routes.push(new PagesRoutes(app));
routes.push(new AuthRoutes(app));
routes.push(new UsersRoutes(app));
routes.push(new ProductRoutes(app));

server.listen(port, () => {
    routes.forEach((route: CommonRoutesConfig) => {
        log(`Routes ${route.getName()}`);
    });
});
