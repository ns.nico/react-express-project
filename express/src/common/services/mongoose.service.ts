import dotenv from 'dotenv';
import mongoose from 'mongoose';

import debug from 'debug';

const log: debug.IDebugger = debug('app:mongoose-service');

dotenv.config();

class MongooseService{

    private URL_MONGO;
    private urlExample;

    constructor(){
        this.urlExample ="mongodb+srv://nksmoney:moneymoney0@nkcluster.ztsnrju.mongodb.net/money?retryWrites=true&w=majority";
        this.URL_MONGO = process.env.URL_MONGO || this.urlExample;
        this.connectWithRetry();
    }

    getMongoose(){
        return mongoose;
    }

    connectWithRetry = () => {
        log('Attempting MongoDB connection (will retry if needed)');
        mongoose
            .connect(this.URL_MONGO, {serverSelectionTimeoutMS: 5000})
            .then(() => {
                log('MongoDB is connected');
            })
            .catch((err: any) => {
                log(`MongoDB connection unsuccessful (will retry after 5000 seconds):`, err);
                setTimeout(this.connectWithRetry, 5000);
            });
    };
}
export default new MongooseService();
