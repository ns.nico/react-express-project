export type Jwt = {
    userId: string;
};

export type JwtAccount = {
    userId: string,
    email: string
}
