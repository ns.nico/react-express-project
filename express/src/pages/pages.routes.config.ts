import express from 'express';
import { CommonRoutesConfig } from '../common/common.routes.config';
import path from 'path';
import debug from 'debug';

const log = debug('pages');

export class PagesRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'UsersRoutes');
    }

    configureRoutes(): express.Application {
        log(__dirname + '../public');
        this.app.use('/', express.static(path.join(__dirname, '../public')));
        return this.app;
    }
}
